## Introduction

This is a test project that is mainly to learn/understand Django (2.1
here) better as well as front-end programming using jQuery.

## Application

Website that allows for users to sign up and register. Users read
NIMEA datastreams from attached GPS devices and upload those tracks,
which are stored with their account.

Users can create/associate a bunch of meters with their tracks and
view it on Google Maps.

## Documentation

Full documentation in ./doc
