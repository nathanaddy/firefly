env:
	python3 -m venv firefly-env
	firefly-env/bin/pip install -r requirements.txt

doc:
	# Output found in doc/_build/html/index.html
	cd ./doc && make html

clean-env:
	rm -fr firefly-env

.PHONY: doc
