# coding: utf-8

"""
Django settings for firefly project.

Generated by 'django-admin startproject' using Django 2.1.5.

For more information on this file, see
https://docs.djangoproject.com/en/2.1/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.1/ref/settings/
"""

import os
from django.core.exceptions import ImproperlyConfigured

def get_secrets_fn():
    curr_dir = os.path.split(__file__)[0]
    return os.path.join(curr_dir, "secrets.txt")

def get_secret_variable(var_name):
    secrets_fn = get_secrets_fn()
    if os.path.isfile(secrets_fn):
        return get_secret_file_variable(var_name)
    else:
        return get_secret_environmental_variable(var_name)

def get_secret_file_variable(var_name):
    try:
        curr_dir = os.path.split(__file__)[0]
        secrets_fn = os.path.join(curr_dir, "secrets.txt")
        secret_dict = dict([map(lambda s: s.strip(), ln.split("=")) for ln in open(secrets_fn).readlines()])
        return secret_dict[var_name]
    except FileNotFoundError:
        error_msg = "Create a secrets.txt file"
        raise ImproperlyConfigured(error_msg)
    except KeyError:
        error_msg = "Set {}=VAL in your secrets.txt file ".format(var_name)
        raise ImproperlyConfigured(error_msg)

def get_secret_environmental_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured(error_msg)

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.1/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+@^n3agt)^plh_q5zy@2*zk38vb2tk#34e-9qnvn^l=dguw6tm'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'firefly.urls'

TEMPLATES = [
 # {
 #   'BACKEND': 'django.template.backends.jinja2.Jinja2',
 #   'DIRS': ['dashboard'],
 #   'APP_DIRS': True,
 #   'OPTIONS': {
 #     'environment': 'firefly.jinja2.environment'
 #   },
 # },
 {
   'BACKEND': 'django.template.backends.django.DjangoTemplates',
   'DIRS': ['dashboard'],
   'APP_DIRS': True,
   'OPTIONS': {
     'context_processors': [
       'django.template.context_processors.debug',
       'django.template.context_processors.request',
       'django.contrib.auth.context_processors.auth',
       'django.contrib.messages.context_processors.messages',
     ],
   },
 },
]

WSGI_APPLICATION = 'firefly.wsgi.application'

# Database
# https://docs.djangoproject.com/en/2.1/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': get_secret_variable('NAME'),
        'USER': get_secret_variable('USER'),
        'PASSWORD': get_secret_variable('PASSWORD'),
        'HOST': get_secret_variable('HOST'),
        'PORT': get_secret_variable('PORT'),
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.1/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/2.1/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.1/howto/static-files/


ROOT_DIR = os.path.join(BASE_DIR, '..')
STATIC_URL = '/static/'

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'),
    os.path.join('dashboard', 'static'),
    os.path.join(ROOT_DIR, 'node_modules')
)

STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
