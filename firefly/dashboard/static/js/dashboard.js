// old-skool javascript
// function myFunction() {
//     document.getElementById("text1").innerHTML = "Hello World!";
// }

function addHighlightClass()  {
    $('div.poem-stanza').addClass('highlight');
    setupGPS();
}

var gps = false;
var gps_iter = 0

function setupGPS()  {
    // Add an event listener on all protocols
    gps.on('data', function(parsed) {
        // console.log(parsed);
        // console.log(gps_iter);

        $("#iter_id").text(gps_iter);
        $(".latitude").text(gps.state.lat);
        $(".longitude").text(gps.state.lon);

        gps_iter++;

    });
}

var gpsData =
    [
        "$GPGGA,051218.560,5231.130,N,01323.061,E,1,12,1.0,0.0,M,0.0,M,,*61",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30",
        "$GPRMC,051218.560,A,5231.130,N,01323.061,E,1437.0,197.4,110119,000.0,W*46",
        "$GPGGA,051219.560,5230.738,N,01322.938,E,1,12,1.0,0.0,M,0.0,M,,*6B",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30",
        "$GPRMC,051219.560,A,5230.738,N,01322.938,E,2439.6,102.6,110119,000.0,W*49",
        "$GPGGA,051220.560,5230.506,N,01323.983,E,1,12,1.0,0.0,M,0.0,M,,*6F",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30",
        "$GPRMC,051220.560,A,5230.506,N,01323.983,E,2339.9,057.3,110119,000.0,W*41",
        "$GPGGA,051221.560,5230.976,N,01324.719,E,1,12,1.0,0.0,M,0.0,M,,*6F",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30",
        "$GPRMC,051221.560,A,5230.976,N,01324.719,E,3450.0,303.1,110119,000.0,W*41",
        "$GPGGA,051222.560,5231.675,N,01323.643,E,1,12,1.0,0.0,M,0.0,M,,*68",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30",
        "$GPRMC,051222.560,A,5231.675,N,01323.643,E,2375.0,244.9,110119,000.0,W*4D",
        "$GPGGA,051223.560,5231.274,N,01322.783,E,1,12,1.0,0.0,M,0.0,M,,*60",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30",
        "$GPRMC,051223.560,A,5231.274,N,01322.783,E,520.4,160.0,110119,000.0,W*79",
        "$GPGGA,051224.560,5231.133,N,01322.835,E,1,12,1.0,0.0,M,0.0,M,,*65",
        "$GPGSA,A,3,01,02,03,04,05,06,07,08,09,10,11,12,1.0,1.0,1.0*30",
        "$GPRMC,051224.560,A,5231.133,N,01322.835,E,520.4,160.0,110119,000.0,W*7C"
    ];

var gpsIndex = 0;


function loadGPSFile() {


}

function simulateGPS() {
    // Call the update routine directly with a NMEA sentence, which would
    // come from the serial port or stream-reader normally

    console.log("Printing " + gpsIndex);
    gps.update(gpsData[gpsIndex]);
    // gps.update("$GPGGA,224900.000,4832.3762,N,00903.5393,E,1,04,7.8,498.6,M,48.0,M,,0000*5E");
    // debugger;
    gpsIndex = (gpsIndex + 1) % gpsData.length;
}

function createPlot() {
    var trace1 = {
        x: [1, 2, 3, 4],
        y: [10, 15, 13, 17],
        type: 'scatter'
    };

    var trace2 = {
        x: [1, 2, 3, 4],
        y: [16, 5, 11, 9],
        type: 'scatter'
    };

    var data = [trace1, trace2];
    Plotly.newPlot('plot', data);
}

$(document).ready(createPlot);
$(document).ready(function foo() {
    gps = new GPS;
});

setInterval(simulateGPS,1000);


function handleFileSelect(evt) {
    var files = evt.target.files; // FileList object

    // use the 1st file from the list
    f = files[0];

    var reader = new FileReader();

    // Closure to capture the file information.
    reader.onload = (function(theFile) {
        return function(e) {

            jQuery( '#ms_word_filtered_html' ).val( e.target.result );
        };
    })(f);

    // Read in the image file as a data URL.
    reader.readAsText(f);
}

$(document).ready(function foo() {
    $("input#upload")[0].addEventListener('change', handleFileSelect, false);
});
