import datetime

from django.http import HttpResponse
from django.shortcuts import render

def index(request):
    context = {
        'name': "Nathan",
        'datestr': datetime.datetime.now().strftime("%H:%M")
    }
    return render(request, 'templates/index.html', context)
